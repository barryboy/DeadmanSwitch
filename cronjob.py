# Deadman's Switch python script made to be run as a cron task

import stat
import os
import time
import datetime
import pyAesCrypt
import subprocess
import logging
import random
import string
from typing import Tuple, List


class DeadmanSwitch:
    """
    Main class.
    """

    def __init__(self):
        """
        Init method of the class.
        """
        self.logPath: str = 'ds.log'
        self.timeDelta: float = 0.1
        self.pwdPath = '/path/toyour/keyfile.txt'
        self.safePath = '/your/folder/path'
        self.payloadPath = '/your/folder/path'
        self.checkFilePath = '/path/toyour/file.extension'
        self.logLevel = logging.DEBUG
        logging.basicConfig(filename=self.logPath, level=self.logLevel, format='%(asctime)s:%(levelname)s:%(message)s')
        self.pwd, self.pwdState = getPwd(self.pwdPath)

    def main(self) -> None:
        """
        Main function.
         """
        if not self.pwdState:
            logging.error('keyfile compromised, initializing panic sequence...')
            self.panic()
            self.pwd = 'Bitch you thought'
            return
        elif self.amIdead():
            self.panic()
            # safeDelete(pwdPath) #TODO disabled file deletion for testing
            self.pwd = 'Bitch you thought'
        else:
            logging.warning('still alive...')

        print('quitting...')
        pass

    def amIdead(self) -> bool:
        """
        Boolean function that executes all checks and return True if checks are not passed.
         """
        now = datetime.datetime.now()
        fileStatsObj = os.stat(self.checkFilePath)
        modificationTimeDirty = time.ctime(fileStatsObj[stat.ST_MTIME])
        print(modificationTimeDirty)
        logging.debug('last modification time: {}'.format(modificationTimeDirty))
        nowFormatted = now.strftime('%a %b %d %H:%M:%S %Y')
        print(nowFormatted)
        logging.debug('actual time: {}'.format(nowFormatted))
        if self.compareTimes(modificationTimeDirty) or self.secondCheck() or self.checkLog():
            return True
        else:
            return False

    def panic(self) -> None:
        """
        Main function that is called when the alarm goes on.
        """
        print('panic!!!')
        logging.error('panic!!!')
        self.encryptSafe()
        time.sleep(5)
        self.decryptPayload()
        print('Panic sequence completed')
        logging.error('Panic sequence completed')
        pass

    def compareTimes(self, oldTime, format='%a %b %d %H:%M:%S %Y') -> bool:
        """
        Boolean function that returns True if the time difference between oldTime and now is bigger than timeCheck.
        """
        # format = '%a %b %d %H:%M:%S %Y'
        timeCheck = datetime.timedelta(hours=self.timeDelta)  # Change here the time before alarm goes off
        old = datetime.datetime.strptime(oldTime, format)
        now = datetime.datetime.now()
        delta = now - old
        print(delta)
        logging.info('Time left: {}'.format(delta))
        if delta > timeCheck:
            return True
        else:
            return False

    def encryptSafe(self) -> None:
        """
        Void function that calls function to encrypt all files in the safe.
        """
        subfolderList, fileList, encryptedFileList = recursiveFileCheck(self.safePath)
        for file in fileList:
            self.encrypt(file)

    def decryptPayload(self) -> None:
        """
        Void function that calls function to decrypt payload.
        """
        sf, fl, encryptedFileList = recursiveFileCheck(self.payloadPath)
        for file in encryptedFileList:
            self.decrypt(file)

    def encrypt(self, file_path) -> None:
        """
        Void function that encrypts given file as filename.ext.aes and calls function to delete original unencrypted.
        """
        bufferSize = 256 * 1024
        print('Encrypting '.format(file_path))
        logging.info('Encrypting {}'.format(file_path))
        pyAesCrypt.encryptFile(file_path, (file_path + '.aes'), self.pwd, bufferSize)
        # safeDelete(file_path) # TODO disabled file deletion for testing

    def decrypt(self, file_path) -> None:
        """
        Void function that decrypts given file.
        """
        bufferSize = 256 * 1024
        print('Decrypting ', file_path)
        logging.info('Decrypting {}'.format(file_path))
        try:
            pyAesCrypt.decryptFile(file_path, file_path + str(random.randint(0, 1000)), self.pwd, bufferSize)
        except ValueError:  # Wrong password or file is corrupted
            logging.warning(f'{file_path} cannot be decrypted, wrong password')

    def checkLog(self) -> bool:
        """
        Bool function that returns True if last logged event is not in timeDelta.
        """
        try:
            with open(self.logPath) as logFile:
                for line in logFile:
                    print(f'line: {line}')
                    pass
                some = line[:16]
            print(f'some: {some}')
            if self.logLevel != 50:
                if self.compareTimes(some, self.timeDelta, format='%Y-%m-%d %H:%M'):
                    return True
            return False
        except FileNotFoundError:
            print('log file not found')
            if self.logLevel == 50:
                print('logging disabled')
                return False
            else:
                logging.error('log file compromised')
                return True

    @staticmethod
    def secondCheck() -> bool:
        """
        Blueprint for other check to implement.
        """
        # TODO secondCheck()
        return False

# Functions outside the class:


def getPwd(pwdPath: str) -> Tuple[str, bool]:
    """
    Tries to read keyfile and handles errors.
    """
    charSet = string.ascii_letters + string.digits + string.punctuation

    try:
        with open(pwdPath, 'r') as pwdObject:
            pwd = pwdObject.read()
    except FileNotFoundError:
        logging.error('keyfile not found')
        pwd = "".join(random.sample(charSet, 32))
        return pwd, False
    return pwd, True


def recursiveFileCheck(path: str) -> Tuple[List, List, List]:
    """
    Recursive function that returns lists of files, encrypted files and subfolders of a given directory.
    """
    subfolders, files, encyptedFiles = [], [], []

    for file in os.scandir(path):
        if file.is_dir():
            subfolders.append(file.path)
        if file.is_file():
            if os.path.splitext(file.name)[1].lower() != '.aes':
                files.append(file.path)
            elif os.path.splitext(file.name)[1].lower() == '.aes':
                encyptedFiles.append(file.path)

    for dir in list(subfolders):
        sf, f, ef = recursiveFileCheck(dir)
        subfolders.extend(sf)
        files.extend(f)
    return subfolders, files, encyptedFiles


def safeDelete(path: str) -> None:
    """
    Void function that executes shred command to safe destroy files and prints stdin stderr outputs of the command.
    """
    logging.info('{} is getting deleted..'.format(path))
    try:
        command = subprocess.run(['shred', '-uvz', '-n 3', path], check=True)

    except FileNotFoundError:   # if command shred not found default to rm -rf
        command = subprocess.run(['rm', '-rf', path], check=True)

    if command.returncode != 0:
        logging.error(' {} exit code status: {}'.format(command, command.returncode))
    else:
        logging.info(' {} exit code status: {}'.format(command, command.returncode))

    print('Original deleted')
    print('exit code status:', command.returncode)
    logging.info(' {} exit code status: {}'.format(command, command.returncode))


# Run this file only if not imported
if __name__ == '__main__':
    run = DeadmanSwitch()
    run.main()
    # print('nerfed')
